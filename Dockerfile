FROM node:7.10 as build-deps
WORKDIR /usr/src/app
COPY package.json ./
COPY build ./build
COPY public ./public
COPY src ./src
COPY scripts ./scripts
COPY config ./config
RUN npm i
EXPOSE 3000
CMD ["npm", "start"]