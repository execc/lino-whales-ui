import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "./widgets/header/Content";
import Main from "./widgets/main/Content";
import Trend from "./widgets/trend/Content";
import UserPage from "./widgets/user/Content";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <BrowserRouter>
          <Switch>
            <Route exact path="/trend" component={Trend} />
            <Route path="/:username" component={UserPage} />
            <Route path="/" component={Main} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
