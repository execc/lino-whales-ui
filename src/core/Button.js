import React, { Component } from "react";
import styled from "styled-components";

const Container = styled.button`
  height: 42px;
  width: 120px;
  border-radius: 5px;
  font-size: 14px;
  background-color: #0098ff;
  color: #fff;

  ${p =>
    p.disabled &&
    `
      color: #000;
      opacity: 0.5;
      background: #fff;
      border: 1px solid #cfd5da;
  `};
`;

class Button extends Component {
  render() {
    return (
      <Container
        disabled={this.props.disabled}
        onClick={this.props.onClick}
        className={this.props.className}
      >
        {this.props.children}
      </Container>
    );
  }
}

export default Button;
