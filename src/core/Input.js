import React, { Component } from "react";
import styled, { css } from "styled-components";

const Wrap = styled.div`
  position: relative;
  display: inline-flex;
  flex-direction: column;
  font-size: 16px;
`;

const Label = styled.label`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  color: #000;
  font-size: 18px;
  font-weight: bold;
  transform: translateY(calc(1em + 3px)) scale(1);
  transition: transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms;
  transform-origin: top left;
  pointer-events: none;

  ${p =>
    p.focused &&
    `
    transform: translateY(0) scale(0.72);
  `};
`;

const HintText = styled.p`
  margin: 1px 0 0;
  color: ${p => (p.error ? "#ff3434" : "#666")};
  font-size: 12px;
`;

const inputStyle = css`
  display: block;
  box-sizing: content-box;
  width: 100%;
  padding: 0 0 0.3em;
  border: 0;
  margin: 0;
  background-color: transparent;
  color: #000;
  font-size: 1em;
`;

const Input = styled.input`
  ${inputStyle};
`;

const InputWrap = styled.div`
  position: relative;
  display: inline-flex;
  padding-top: 20px;

  ::before {
    content: " ";
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    border-bottom: 1px solid
      ${p => (p.error ? "#ff3434" : p.focused ? "#000" : "#979797")};
    pointer-events: none;
  }

  :hover {
    ::before {
      border-bottom: 1px solid ${p => (p.error ? "#ff3434" : "#000")};
    }
  }
`;

const withMaterial = Input =>
  class extends Component {
    static defaultProps = {
      onFocus: Function.prototype,
      onBlur: Function.prototype,
      onChange: Function.prototype,
      autoCorrect: "off",
      spellCheck: "false"
    };

    state = {
      focused: false
    };

    onFocus = e => {
      const { onFocus } = this.props;
      this.setState({ focused: true });
      onFocus(e);
    };

    onBlur = e => {
      const { onBlur } = this.props;
      this.setState({ focused: false });
      onBlur(e);
    };

    render() {
      const {
        value,
        labelText,
        hintText,
        id,
        error,
        onChange,
        onFocus,
        onBlur,
        className,
        ...rest
      } = this.props;
      const { focused } = this.state;
      return (
        <Wrap className={className}>
          <Label htmlFor={id} focused={focused || value}>
            {labelText}
          </Label>
          <InputWrap error={error} focused={focused}>
            <Input
              value={value}
              id={id}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              onChange={onChange}
              {...rest}
            />
          </InputWrap>
          {hintText && <HintText error={error}>{hintText}</HintText>}
        </Wrap>
      );
    }
  };

export default withMaterial(Input);
