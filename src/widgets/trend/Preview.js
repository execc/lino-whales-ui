import React, { Component } from "react";
import styled from "styled-components";
import Chart from "chart.js";
import dlive from "../assets/dlive.png";

const Wrap = styled.div`
  display: flex;
  margin-bottom: 15px;
  align-items: center;
  justify-content: space-around;
`;

const Data = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.div`
  font-size: 36px;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const Subtitle = styled.div`
  font-size: 30px;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const Description = styled.div`
  font-size: 18px;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const Hr = styled.hr`
  width: 100%;
`;

const CanvasWrap = styled.div``;

const Canvas = styled.canvas``;

const Image = styled.div`
  width: 200px;
  height: 200px;
  background: #fff url(${dlive}) 0 / 200px 200px no-repeat;
`;

const data = {
  datasets: [
    {
      data: [22, 27, 36, 15],
      backgroundColor: ["#ffcd33", "#ff5382", "#0088c8", "green"]
    }
  ],
  labels: ["x < 50", "50 < x < 100", "100 < x < 500", "x < 500"]
};

class Preview extends Component {
  componentDidMount() {
    var ctx = document.getElementById("myChart");

    new Chart(ctx, {
      type: "pie",
      data: data
    });
  }

  render() {
    return (
      <Wrap>
        <Image />
        <Data>
          <Title>LinoWhales</Title>
          <Hr />
          <Subtitle>Ranking by «Lino»</Subtitle>
          <Description>Total «Lino»: 14902</Description>
          <Description>Average «Lino»: 304</Description>
        </Data>
        <CanvasWrap>
          <Canvas id="myChart" height="350" width="350" />
        </CanvasWrap>
      </Wrap>
    );
  }
}

export default Preview;
