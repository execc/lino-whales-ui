import React, { Component } from "react";
import styled from "styled-components";
import Filters from "./Filters";
import Table from "./Table";

const Wrap = styled.div`
  padding: 15px 30px;
`;

export default class Content extends Component {
  state = {
    content: [],
    params: {},
    period: 7
  };

  fetcher(
    period = 7,
    page = 0,
    pageSize = 15,
    asc = "false",
    criteria = "lino"
  ) {
    const that = this;
    fetch(
      `/api/queryTrending?period=${period}&page=${page}&pageSize=${pageSize}&criteria=${criteria}`
    )
      .then(function(response) {
        return response.json();
      })
      .then(function(res) {
        const { content, ...rest } = res;
        that.setState({ content: content, params: rest });
      });
  }

  componentDidMount() {
    this.fetcher();
  }

  handleTrendChange = event => {
    this.fetcher(event.target.value);
    this.setState({ period: event.target.value });
  };

  render() {
    return (
      <Wrap>
        <Filters
          trend={this.state.period}
          trendChange={this.handleTrendChange}
          {...this.props}
        />
        <Table
          content={this.state.content}
          params={this.state.params}
          {...this.props}
        />
      </Wrap>
    );
  }
}
