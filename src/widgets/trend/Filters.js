import React, { Component } from "react";
import styled from "styled-components";
// import Input from "@material-ui/core/Input";
// import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ASelect from "@material-ui/core/Select";

// const Wrap = styled.div`
//   display: flex;
//   justify-content: space-between;
//   margin-bottom: 15px;
// `;

// const ButtonsWrap = styled.div`
//   display: flex;
// `;

// const ButtonWrap = styled.div`
//   border-radius: 5px;
//   margin-right: 5px;
//   border: 1px solid #000;
// `;

const Select = styled(ASelect)`
  margin-right: 20px;
  min-width: 120px;
  color: #fff;
  margin-bottom: 10px;
`;

class Filters extends Component {
  render() {
    const { trend, trendChange } = this.props;

    return (
      <div>
        <div>
          <FormControl>
            <InputLabel htmlFor="age-simple">Trend</InputLabel>
            <Select
              value={trend}
              onChange={trendChange}
              inputProps={{
                name: "trend",
                id: "trend"
              }}
            >
              <MenuItem value={3}>3 days</MenuItem>
              <MenuItem value={7}>7 days</MenuItem>
              <MenuItem value={14}>2 week</MenuItem>
              <MenuItem value={30}>1 month</MenuItem>
            </Select>
          </FormControl>
        </div>
      </div>
    );
  }
}

export default Filters;
