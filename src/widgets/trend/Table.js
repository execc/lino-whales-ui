import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import styled from "styled-components";

const Link = styled.div`
  cursor: pointer;
  color: #09abbf;
`;

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#f2f2f2",
    color: theme.palette.common.black,
    padding: "4px 10px 4px 10px"
  },
  body: {
    fontSize: 14,
    whiteSpace: "nowrap",
    padding: "4px 10px 4px 10px"
  }
}))(TableCell);

const styles = theme => ({
  root: {
    width: "100%",
    minWidth: 700,
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
});

export default class CustomTable extends Component {
  render() {
    const { content } = this.props;
    return (
      <Paper className={styles.root}>
        <Table className={styles.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell numeric>Rank</CustomTableCell>
              <CustomTableCell>Username</CustomTableCell>
              <CustomTableCell>Lino</CustomTableCell>
              <CustomTableCell>Locked points</CustomTableCell>
              <CustomTableCell>Posts</CustomTableCell>
              <CustomTableCell>Diff</CustomTableCell>
              <CustomTableCell>Min value</CustomTableCell>
              <CustomTableCell>Max value</CustomTableCell>
              <CustomTableCell>Original income</CustomTableCell>
              <CustomTableCell>Friction income</CustomTableCell>
              <CustomTableCell>Inflation income</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {content.map((row, index) => {
              return (
                <TableRow className={styles.row} key={row.id.username}>
                  <CustomTableCell numeric>#{index + 1}</CustomTableCell>
                  <CustomTableCell>
                    <Link
                      onClick={() =>
                        this.props.history.push("/" + row.id.username)
                      }
                    >
                      {row.id.username}
                    </Link>
                  </CustomTableCell>
                  <CustomTableCell>{row.lino}</CustomTableCell>
                  <CustomTableCell>{row.lp}</CustomTableCell>
                  <CustomTableCell>{row.posts}</CustomTableCell>
                  <CustomTableCell>{row.diff}</CustomTableCell>
                  <CustomTableCell>{row.min}</CustomTableCell>
                  <CustomTableCell>{row.max}</CustomTableCell>
                  <CustomTableCell>{row.originalIncome}</CustomTableCell>
                  <CustomTableCell>{row.frictionIncome}</CustomTableCell>
                  <CustomTableCell>{row.inflationIncome}</CustomTableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}
