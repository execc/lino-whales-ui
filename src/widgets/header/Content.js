import React, { Component } from "react";
import AContainer from "../../core/Container";
import styled from "styled-components";

const Title = styled.a`
  width: 150px;
  color: #fffb00;
  text-decoration: none;
  font-size: 30px;
  font-weight: bold;
  cursor: pointer;
`;

const Container = styled(AContainer)`
  max-width: 100%;
  padding: 15px 40px;
  border-radius: 0;
  background-color: #000;
`;

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Trend = styled.a`
  color: #fffb00;
  text-decoration: none;
  cursor: pointer;
`;

class Header extends Component {
  render() {
    return (
      <Container>
        <Wrap>
          <Title href="/">Lino</Title>
          <Trend href="/trend">Trending</Trend>
        </Wrap>
      </Container>
    );
  }
}

export default Header;
