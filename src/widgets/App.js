import React, { Component } from "react";
import Container from "../core/Container";
import Main from './main/Content';

class App extends Component {
  componentWillMount() {
    console.log(this.props);
    if (!this.props.match.params.crowdsaleId)
      this.props.history.push("/settings");
    else if (!this.props.user) {
      this.props.history.push("/auth");
    }
  }

  render() {
    return (
      <Container>
        <Main {...this.props} />
      </Container>
    );
  }
}

export default App;
