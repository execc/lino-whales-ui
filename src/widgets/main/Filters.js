import React, { Component } from "react";
import styled from "styled-components";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";

const Wrap = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 15px;
`;

const ButtonsWrap = styled.div`
  display: flex;
`;

const ButtonWrap = styled.div`
  border-radius: 5px;
  margin-right: 5px;
  border: 1px solid #000;
`;

class Filters extends Component {
  state = {
    ranking: "",
    trend: ""
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { fetcher, params, history } = this.props;
    return (
      <Wrap>
        <Input
          placeholder="Search rankings"
          onChange={e => this.setState({ ranking: e.target.value })}
          onKeyPress={e => {
            if (e.key === "Enter") {
              history.push("/" + this.state.ranking);
            }
          }}
        />
        <ButtonsWrap>
          {!params.first && (
            <ButtonWrap>
              <Button onClick={() => fetcher(params.number - 1)}>Prev</Button>
            </ButtonWrap>
          )}
          {!params.last && (
            <ButtonWrap>
              <Button onClick={() => fetcher(params.number + 1)}>Next</Button>
            </ButtonWrap>
          )}
        </ButtonsWrap>
      </Wrap>
    );
  }
}

export default Filters;
