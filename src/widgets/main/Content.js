import React, { Component } from "react";
import styled from "styled-components";
import Preview from "./Preview";
import Filters from "./Filters";
import Table from "./Table";

const Wrap = styled.div`
  padding: 15px 30px;
`;

class Content extends Component {
  state = {
    content: [],
    params: {}
  };

  fetcher(page = 0, pageSize = 15, asc = "false", criteria = "lino") {
    const that = this;
    fetch(
      `/api/queryCurrentStat?page=${page}&pageSize=${pageSize}&asc=${asc}&criteria=${criteria}`
    )
      .then(function(response) {
        return response.json();
      })
      .then(function(res) {
        const { content, ...rest } = res;
        that.setState({ content: content, params: rest });
      });
  }

  componentDidMount() {
    this.fetcher();
  }

  handleChange = value => {
    this.fetcher(value);
  };

  render() {
    return (
      <Wrap>
        <Preview params={this.state.params} {...this.props} />
        <Filters
          params={this.state.params}
          fetcher={this.handleChange}
          {...this.props}
        />
        <Table
          content={this.state.content}
          params={this.state.params}
          {...this.props}
        />
      </Wrap>
    );
  }
}

export default Content;
