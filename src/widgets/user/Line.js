import React, { Component } from "react";
import Chart from "chart.js";
import styled from "styled-components";

const CanvasWrap = styled.div`
  margin-bottom: 30px;
`;

const Canvas = styled.canvas``;

class Line extends Component {
  componentDidMount() {
    var ctx = document.getElementById("myChart");

    let datasetLino = [];
    let labels = [];

    const { content = [] } = this.props;

    content.forEach(element => {
      datasetLino.splice(0, 0, element.lino);
      labels.splice(0, 0, element.id.date);
    });

    const data = {
      labels: labels,
      datasets: [
        {
          label: "Lino",
          data: datasetLino,
          borderColor: "#ffeb3b",
          fill: false
        }
      ]
    };

    if (ctx)
      new Chart(ctx, {
        type: "line",
        data: data,
        options: {
          elements: {
            line: {
              tension: 0 // disables bezier curves
            }
          }
        }
      });
  }

  render() {
    const { content = [] } = this.props;
    return content.length ? (
      <div>
        <CanvasWrap>
          <Canvas id="myChart" height="150" width="1000" />
        </CanvasWrap>
      </div>
    ) : null;
  }
}

export default Line;
