import React, { Component } from "react";
import styled from "styled-components";

const Wrap = styled.div`
  display: flex;
  margin-bottom: 15px;
  align-items: center;
  justify-content: space-around;
`;

const Data = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.div`
  font-size: 36px;
  font-weight: bold;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const Description = styled.div`
  font-size: 18px;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const Hr = styled.hr`
  width: 100%;
`;

class Preview extends Component {
  render() {
    const { content = [] } = this.props;
    const currentInfo = content[0];
    return currentInfo ? (
      <Wrap>
        <Data>
          <Title>{currentInfo.id.username}</Title>
          <Hr />
          <Description>Total «Lino»: {currentInfo.lino}</Description>
          <Description>Average «Locked points»: {currentInfo.lp}</Description>
        </Data>
      </Wrap>
    ) : null;
  }
}

export default Preview;
