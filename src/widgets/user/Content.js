import React, { Component } from "react";
import styled from "styled-components";
import Info from "./Info";
import Line from "./Line";
import Table from "./Table";

const Wrap = styled.div`
  padding: 15px 30px;
`;

const Title = styled.div`
  font-size: 36px;
  font-weight: bold;
  margin: 20px auto;
  text-align: center;
`;

export default class Content extends Component {
  state = {
    content: [],
    params: {}
  };

  fetcher(page = 0, pageSize = 15, asc = "false") {
    const that = this;
    fetch(
      `/api/queryHistory?user=${
        this.props.match.params.username
      }&page=${page}&pageSize=${pageSize}&asc=${asc}`
    )
      .then(function(response) {
        return response.json();
      })
      .then(function(res) {
        const { content, ...rest } = res;
        that.setState({ content: content, params: rest });
      });
  }

  componentDidMount() {
    this.fetcher();
  }

  render() {
    return (
      <Wrap>
        {this.state.content.length ? (
          <div>
            <Info content={this.state.content} />
            <Line content={this.state.content} />
            <Table
              content={this.state.content}
              params={this.state.params}
              {...this.props}
            />
          </div>
        ) : (
          <Title>User "{this.props.match.params.username}" not found</Title>
        )}
      </Wrap>
    );
  }
}
